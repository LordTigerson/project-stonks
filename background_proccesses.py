## Module imports
from os_core import gettimenow
from os_core import gettimeHour
from os_core import gettimeMinute
from os_core import gettimeSecond
from os_core import getweekdayToday
from os_core import clearscreen
from os_core import singleyamlvarread
from trading_core import getstockvolume
from trading_core import getstockprice
from trading_core import getbackgroundproccesses
from trading_core import getdaymostactive
from database_core import CheckTableReality
from database_core import CreateStockTable
from database_core import ReadStockList
from database_core import WriteToStockList
from database_core import WriteToStock
from database_core import GetCount
from threading import Thread
import time

##------------------------------------------------------------------------------------------------------------------------------
## Silly Boot Up Logo
def Loading():
    version = singleyamlvarread('Version')
    clearscreen()  
    print('______          _           _     _____ _              _        ')
    print('| ___ \\        (_)         | |   /  ___| |            | |       ')
    print('| |_/ / __ ___  _  ___  ___| |_  \ `--.| |_ ___  _ __ | | _____ ')
    print('|  __/  __/ _ \| |/ _ \/ __| __|  `--. \ __/ _ \|  _ \| |/ / __|')
    print('| |  | | | (_) | |  __/ (__| |_  /\__/ / || (_) | | | |   <\__ \\')
    print('\_|  |_|  \___/| |\___|\___|\__| \____/ \__\___/|_| |_|_|\_\___/')
    print(f'              _/ |                                   Beta V{version}')
    print('             |__/                                               ')         

def Error():
    clearscreen()
    print(' _____                           __')
    print('|  ___|                     _   / /')
    print('| |__ _ __ _ __ ___  _ __  (_) | | ')
    print('|  __|  __|  __/ _ \|  __|     | | ')
    print('| |__| |  | | | (_) | |     _  | | ')
    print('\\____/_|  |_|  \\___/|_|    (_) | | ')
    print('                                \\_\\')
                                                                                                      

##------------------------------------------------------------------------------------------------------------------------------
## Top Menu Display
##
## delay    | int   | Variable is used to show the amount of time that will elapse between cycle 
def Display(delay,marketplaceopenclose):    
    if marketplaceopenclose == True:
        marketplaceopenclose = 'Open'
    else:
        marketplaceopenclose = 'Closed'
    if delay < 60:
        delaymessage = f'{delay} seconds'
    elif delay < 3600:
        delay = delay/60
        delaymessage = f'{delay} minutes'
    else:
        delay = ((delay/60)/60)
        delaymessage = f'{delay} hours'
    print(f'> Next cycle check in {delaymessage}')
    print(f'> Market Place is currently {marketplaceopenclose}')
    print(f'> Last Scan at: {gettimenow()}\n')

def DelayLoader(OpenMarketDelay,ClosedMarketDelay):
    current_hour = gettimeHour()
    weekday = getweekdayToday()
    if weekday == 0 or weekday == 1 or weekday == 2 or weekday == 3 or weekday == 4:
        if current_hour == '15' or current_hour == '16' or current_hour == '17' or current_hour == '18' or current_hour == '19' or current_hour == '20' or current_hour == '21':
            return OpenMarketDelay, True
        else:  
            return ClosedMarketDelay, False
    else:
        return 21600, False

##------------------------------------------------------------------------------------------------------------------------------
## The Top 100 Most Active stock of the last 24 hr are gathered as a list. Once this list is generated it will create DB Tables
## from template from StockTemplate as well as save any new stocks into DB Table StockList
def AddMostActiveStocks():
    data = getdaymostactive()
    count = 0
    while count != 50:
        clearscreen()
        print(f'> Checking Stock List {data[count]}')
        if CheckTableReality(data[count]) == False and data[count] != 'KEY' and data[count] != 'TERP':
            print(f'> Adding to stock list')
            CreateStockTable(data[count])          
            WriteToStockList(data[count],'')
            time.sleep(3)
        count+=1

##------------------------------------------------------------------------------------------------------------------------------
## Main
def MainFunction():

    ## Variable Declaration
    firsttimelaunch = False ##----------------------------------- Set this variable to True if this is your first time launching this
    StockAutoDiscovery = singleyamlvarread('StockAutoDiscover')
    StockClear = False
    volume = ''
    stockopen = ''
    stockclose = ''
    dayrange = ''
    yearest = ''
    minute = ''
    OpenMarketDelay = singleyamlvarread('OpenMarketDelay')
    ClosedMarketDelay = singleyamlvarread('ClosedMarketDelay')

    Loading()
    time.sleep(1)
    while True:
        clearscreen()
        Loading()
        delay,markeopenclose = DelayLoader(OpenMarketDelay,ClosedMarketDelay)
        Display(delay,markeopenclose)

        if minute != gettimeMinute():
            minute = gettimeMinute()
            StockList = ReadStockList('StockList')

            if firsttimelaunch == True:
                print('> Checking Most Active Stocks')
                AddMostActiveStocks()
                StockList = ReadStockList('StockList')
                firsttimelaunch = False
            else:
                for stock in StockList.splitlines():
                    str(stock)
                    stock = stock[1:-2]        
                    
                    stockpricenow,volume,stockopen,stockclose,dayrange,yearest = getbackgroundproccesses(stock)
                    thread = Thread(target = WriteToStock, args =(stock, volume, stockpricenow, stockopen, stockclose, dayrange, yearest)) 
                    thread.start()        
                    print(f'> Writing data for the stock {stock}') 
                    time.sleep(1)

            if gettimeHour() == '2' and StockClear == False and StockAutoDiscovery == True:
                print('> Checking Most Active Stocks')
                AddMostActiveStocks()
                StockList = ReadStockList('StockList')
                StockClear = True
            elif gettimeHour() == '3':
                StockClear = False

            time.sleep(delay)
        else:
            message = (60-int(gettimeSecond()))
            print(f'> Next scan starting in {message}')
            
        time.sleep(2)