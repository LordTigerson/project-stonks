import pyodbc 

def conConnect():
    global conn
    global cursor
    conn = pyodbc.connect('Driver={SQL Server};'
                        'Server=localhost\SQLEXPRESS;'
                        'Database=ProjectStonks;'
                        'Trusted_Connection=yes;')

    cursor = conn.cursor()

def WriteToStockList(DBtable,StockName):
    global conn
    global cursor    
    iD = GetCount('StockList') + 1
    conConnect()
    cursor.execute(f'''INSERT INTO ProjectStonks.dbo.StockList(id, stockcode, stockname) VALUES ({iD}, '{DBtable}','{StockName}')''')                    
    conn.commit()
    conn.close()

def WriteToStock(DBtable, volume, currentstock, openprice, closeprice, dayrange, yearestimate):
    global conn
    global cursor    
    iD = GetCount(DBtable) + 1
    conConnect()
    cursor.execute(f'''INSERT INTO ProjectStonks.dbo.{DBtable}(id, volume, current_stock, datetimetable, open_price, close_price, year_estimate) VALUES ({iD}, {volume}, {currentstock}, GETUTCDATE(), {openprice}, {closeprice}, {yearestimate})''')                    
    conn.commit()
    conn.close()

def ReadStockList(DBtable):    
    global cursor
    StockList=''
    conConnect()
    cursor.execute(f'SELECT StockCode FROM ProjectStonks.dbo.{DBtable}')
    for row in cursor:
        if StockList == '':
            StockList = row
        else:
            StockList = f'{StockList}\n{row}'
    conn.close()
    StockList = StockList.translate({ord(i): None for i in ',()'})
    return StockList

def CheckTableReality(DBtable):
    global cursor
    try:
        conConnect()
        cursor.execute(f'SELECT * FROM ProjectStonks.dbo.{DBtable}')
        conn.close()
        return True
    except:
        return False

def CreateStockTable(DBtable):    
    global cursor
    conConnect()
    cursor.execute(f'SELECT * INTO {DBtable} FROM ProjectStonks.dbo.StockTemplate')
    cursor.commit()
    conn.close()

def GetCount(DBtable):
    global cursor
    conConnect()
    cursor.execute(f'SELECT COUNT(*) FROM ProjectStonks.dbo.{DBtable}')
    for row in cursor:
        data = str(row)
        data = data.translate({ord(i): None for i in ',()'})
        data = int(data)
        #conn.close()
        return data

def printTable(DBtable):
    global cursor
    conConnect()
    cursor.execute(f'SELECT * FROM ProjectStonks.dbo.{DBtable}')
    for row in cursor:
        print(row)
    conn.close()