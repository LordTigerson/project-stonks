from database_core import CreateStockTable
from database_core import WriteToStockList
from os_core import clearscreen
from os_core import checkfilestate
from os_core import singleyamlvarread
from background_proccesses import Loading
from background_proccesses import Error
from background_proccesses import MainFunction

def StockManualEntry():
    clearscreen()
    print('Please enter stock code')
    StockCode = input('> ')
    print('Please enter Company name')
    StockName = input('> ')
    clearscreen()
    print(f'Details entering:\nStock Code : {StockCode}\nCompany Name : {StockName}\n\nSave y/n')
    saveinput = input('> ')
    if saveinput == 'y':
        CreateStockTable(StockCode)          
        WriteToStockList(StockCode,StockName)
        clearscreen()
        input('Save Complete <enter>')
    clearscreen()

def Initializer():
    configfilestate = checkfilestate('config.yaml')

    if configfilestate == False:
        Error()
        input('Missing Config file. Unable to proceed.')
        exit()

while True:
    clearscreen()
    Loading()
    AutoDiscover = singleyamlvarread('StockAutoDiscover')
    if AutoDiscover == False:
        AutoDiscover = 'Disabled'
    else:
        AutoDiscover = 'Enabled'
    print(f'[1] Background Procceses')
    print(f'[2] Add Manual Stock')
    print(f'[3] Auto Discover ({AutoDiscover})\n')
    print(f'[0] Exit')
    UserInput = input('\n> ')
    print()

    if UserInput == '1':
        MainFunction()
    elif UserInput == '2':
        StockManualEntry()
    elif UserInput == '3':
        input('This function is currently not available <return>')
    elif UserInput == '0':
        exit()
    else:
        input('Unrecognized Commands <return>')