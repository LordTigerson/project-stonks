import os
import socket
import datetime
import time
import yaml

def clearscreen():
    os.system('cls' if os.name == 'nt' else 'clear')

def gethostname():
    hotsname = socket.gethostname()
    return hotsname

def checkfilestate(path):
    state = os.path.isfile(path)
    return state

def singleyamlvarread(Variable):
    with open(r'config.yml') as file:
        doc = yaml.load(file, Loader=yaml.FullLoader)
        return doc[Variable]

def gettimenow():
    t = time.localtime()
    return time.strftime("%H:%M", t)

def gettimeHour():
    t = time.localtime()
    return time.strftime("%H", t)

def gettimeMinute():
    t = time.localtime()
    return time.strftime("%M", t)

def gettimeSecond():
    t = time.localtime()
    return time.strftime("%S", t)

def getweekdayToday():
    return datetime.datetime.today().weekday()