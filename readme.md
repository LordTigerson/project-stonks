# Project Stonks

## Current Version:
* Public Beta V1.012
## Current Objective: 
* Creating an XML Logging system
* Data Gathering
* Fix Bugs and glitches with updates or label them as features

## Updates:
* Script can now tell the day of the week.
* Increased delay in scans on weekends to 2 hours per cycle.
* Notification bug fixes.
* Created menu system in launcher.
* Created config.yml
* Script now reads variables from config.yml
* Quality of life improvements.
* Script will now switch how it monitors depending if stock is open or closed.
* Fixed bugs that were left for another time.
* Implemented new stock skip scan if less than 60 seconds have passed since last scan.
* Added ignore stock add (there is actualy a bug with certain stock returning null but it's being branded as a freature).
* Added some bugs to fix at a later stage.
* Exported Grafana Dashboard located in folder Grafana Dashboards/Stock Data Check.json
* Updated stock ignore list that was freezing the script when no data was returned.
* Changed how DateTime is saved from script into Stock Tables to be compliant with grafana plotting.
* Implemented multi threading for Funtion 'WriteToStock' to improve speed when data is saved.

## Scope:
To produce a python scipt capable of identifying, buying and selling stock with a profit.

# Important
If you are using this script for the first time, make sure to set FirstTimeRun Variable as True in background_proccesses.py in order to populate the database with Stock Tables.

## How to run:
```bash
python3.6 launcher.py
```

## How does it work:
## Launcher
The purpose of launcher is to act as a menu function for the tool. Currently it has 3 functions available.
* Start Background process
* Manualy add stock to Stock List
* Enable/Disable Stock Auto-discovery function (Currently Disabled due to glitches)

### Function 1 Data Gathering
Every Cycle the script will gather stock information in the following order:
Stock Volume Bought, Stock Price, Date of Information Gathering, Time of Information Gathering, Stock Open Price, Stock Last Closing Price, Estimated Stock Price, 1 Year Estimated Value.
All data is logged through SQL Express.

### Function 2 Manualy add stocks
When selecting this option you will be asked to enter the code of the stock you want to scan (example AAPL). Afterwords it will ask you to enter the name of the stock (example Apple). Finaly the script will show you the data you are trying to enter one last time to confirm and changes will be commited. This function can run in parallel with Background processes and any new stock will be picked up on the next cycle.

### Function 3 Check Most Active Stocks
At 2AM in the morning the script will automaticaly gather logs for the top 100 most active stocks in the last 24hours. Once the stocks lists are gathered the script will automaticaly compare with the stocks it currently knows, and if any particular stocks are found missing the script will automaticaly create the neccassary directories and file structures so that it can start gathering data on them aswell.

A full list of all Stock can be found by executing the folloeing command on SSMS:
```sql
SELECT * FROM StockList NOLOCK
ORDER BY id Asc
```

## Software Requirements
* Python3
* SQL Express

### Python3 Module Requirements:
```bash
pip3 install yahoo_fin
pip3 install stockquotes
pip3 install pandas
pip3 install pyodbc
pip3 install pyyaml
```

### SQL Table Requirements:
DB Name 'ProjectStonks'

##### Table 1 Name: StockList
| Name      | Type | Allowed Null | Primary Key |
|-----------|------|--------------|-------------|
| id        | int  | False        | Yes         |
| stockcode | text | False        |             |
| stockname | text | True         |             |

##### Table 2 Name: StockTemplate
| Name          | Type     | Allowed Null | Primary Key |
|---------------|----------|--------------|-------------|
| id            | int      | False        | Yes         |
| volume        | int      | False        |             |
| current_stock | money    | False        |             |
| datetimetable | datetime | False        |             |
| open_price    | money    | False        |             |
| close_price   | money    | False        |             |
| year_estimate | money    | False        |             |

## Hardware Requirements:
For the time being, any OS that runs python3. However SQL Express can only run on a Windows Environment.