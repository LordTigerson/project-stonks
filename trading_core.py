from yahoo_fin import stock_info as si
from yahoo_fin.stock_info import get_day_most_active
from database_core import WriteToStock
from threading import Thread
from os_core import clearscreen
import stockquotes
import wallet_core
import time
import pyodbc 


def getstockprice(stock,StockEngine,Trade):
    loop = True
    count = 100
    if StockEngine == 1:
        while loop == True:
            try:
                ammount = si.get_live_price(stock)
                loop == False
                return ammount
            except:
                clearscreen()
                print('Timeout from Yahoo Finance API')    
                count-=1
                time.sleep(5)
                clearscreen()
                print('Trying again')
            if count == 0 and Trade == True:
                wallet_core.sellstock(count)
                Trade = False
        return ammount
    elif StockEngine == 2:
        while loop == True:
            try:
                kroger = stockquotes.Stock(stock)
                krogerPrice = kroger.current_price
                return krogerPrice
            except:
                clearscreen()
                print('Timeout from Kroger Finance API')    
                count-=1
                time.sleep(5)
                clearscreen()
                print('Trying again')

def getstocktable(stock):
    Loop = True
    count = 100
    while Loop == True:
        try:
            table = si.get_quote_table(stock)
            Loop = False
        except:
            print('Timeout from Yahoo Finance API')
            count-=1
            time.sleep(60)
        if count == 0:
            Loop = False
            return 'Null'
    return table

def getdaymostactive():
    Loop = True
    count = 100
    while Loop == True:
        try:
            data = get_day_most_active()
            Loop = False
            data = data["Symbol"]
            return data
        except:
            print('Timeout from Yahoo Finance API')
            count-=1
            time.sleep(5)

def getstockvolume(stock):
    table=getstocktable(stock)
    table = table["Volume"]  
    return table

def getstockopen(stock):
    table=getstocktable(stock)
    table = table["Open"]  
    return table

def getstockpreviouseclose(stock):
    table=getstocktable(stock)
    table = table["Previous Close"]
    return table

def getstockdaysrange(stock):
    table=getstocktable(stock)
    table = table["Day's Range"]
    return table

def getstock1yEst(stock):
    table=getstocktable(stock)
    table = table["1y Target Est"]
    return table

def getbackgroundproccesses(stock):
    table=getstocktable(stock)
    stockprice=table["Quote Price"]
    volume=table["Volume"]
    stockopen=table["Open"]
    stockclose=table["Previous Close"]
    dayrange = table["Day's Range"]
    yearest = table["1y Target Est"]        
    return stockprice,volume,stockopen,stockclose,dayrange,yearest